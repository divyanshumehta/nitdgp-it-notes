#include<stdio.h>
#include<ctype.h>
#include<string.h>

void keyw(char *p, int);
int i=0;

char keywords[32][10]={"auto","break","case","char","const","continue","default",
"do","double","else","enum","extern","float","for","goto",
"if","int","long","register","return","short","signed",
"sizeof","static","struct","switch","typedef","union",
"unsigned","void","volatile","while"};
int main()
{
    char ch,str[25],punc_mark[16]=" \t\n,;(){}[]#\"<>",oper[]="!%^&*-+=~|.<>/?",arguments[100][500];
    int j, line_no=1;
    char fname[50];
    FILE *f1;
    printf("Enter file: ");
    scanf("%s",fname);
    f1 = fopen(fname,"r");
    if(f1==NULL)
    {
     printf("file not found");
     exit(0);
    }
    while((ch=fgetc(f1))!=EOF)
    {
    for(j=0;j<=14;j++)
    {
        if(ch==oper[j])
        {
            if (ch=='!' || ch=='>' || ch == '<')
            {
                printf("%c",ch);
                ch=fgetc(f1);
                if (ch=='=')
                {
                    printf ("\t\t RELATIONAL OPERATOR \t\t %d\n", line_no++);
                }
                else
                    printf ("\t\t RELATIONAL OPERATOR\n");
            }
            else if (ch=='&')
            {
                printf("%c",ch);
                ch=fgetc(f1);
                if(ch=='&')
                printf("%c\t\t LOGICAL OPERATOR \t\t %d\n",ch, line_no++);
                else printf("\t\t OPERATOR\n");
            }
            else if (ch=='|')
            {
                printf("%c",ch);
                ch=fgetc(f1);
                if(ch=='|')
                printf("%c\t\t LOGICAL OPERATOR\t\t %d\n",ch, line_no++);
                else printf("\t\t OPERATOR\t\t %d\n", line_no++);
            }
            else if (ch=='=')
            {
                printf("%c",ch);
                ch=fgetc(f1);
                if (ch=='=')
                    printf("%c\t\t RELATIONAL OPERATOR\t\t %d\n",ch, line_no++);
                else
                    printf("\t\t ASSIGNMENT OPERATOR\t\t %d \n", line_no++);
            }
            if (ch=='+')
            {
                printf("%c",ch);
                ch=fgetc(f1);
                if(ch=='+') printf("%c\t\t INCREMENT OPERATOR\t\t %d \n",ch, line_no++);
                else printf("\t\t ARITHMETIC OPERATOR\t\t %d\n", line_no++);
            }
            if (ch=='-')
            {
                printf("%c",ch);
                ch=fgetc(f1);
                if(ch=='-')
                    printf("%c\t\t DECREMENT OPERATOR\t\t %d\n",ch, line_no++);
                else
                    printf("\t\t ARITHMETIC OPERATOR\t\t %d\n", line_no++);
            }
                   // printf("%c \t  OPERATOR\n",ch);
            str[i]='\0';
            keyw(str, line_no);
        }
    }
    for(j=0;j<=14;j++)
    {
        if(i==-1)
            break;
        if(ch==punc_mark[j])
        {
            if(ch=='#')
            {
                printf("\n\n");
                while(ch!='>')
                {
                    printf("%c",ch);
                    ch=fgetc(f1);
                }
                printf("%c\t\t HEADER FILE\t\t %d\n",ch, line_no++);
                i=-1;
                break;
            }
            if(ch=='"')
            {
                do
                {
                    ch=fgetc(f1);
                    printf("%c",ch);
                }while(ch!='"');
                printf("\b\t\t ARGUMENT\t\t %d\n", line_no++);
                i=-1;
                break;
            }
            if(ch=="\n")
            {
                line_no++;
            }
            str[i]='\0';
            keyw(str, line_no);
        }
    }
    if(i!=-1)
    {
        str[i]=ch;
        i++;
    }
    else
        i=0;
    }
    return 0;
}

void keyw(char *p, int line_no)
{
    int k,flag=0;
    for(k=0;k<=31;k++)
    {
        if(strcmp(keywords[k],p)==0)
        {
            printf("%s\t\t KEYWORD\t\t %d\n",keywords[k], line_no++);
            flag=1;
            break;
        }
    }
    if(flag==0)
    {
        if(isdigit(p[0]))
        {
            printf("%s\t\t DIGIT\t\t %d\n",p, line_no++);
        }
        else
        {
            if(p[0]!='\0')
            {
                printf("%s\t\t IDENTIFIER\t\t %d\n",p, line_no++);
            }
        }
    }
    i=-1;
}
