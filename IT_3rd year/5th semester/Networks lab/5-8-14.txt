communication with mac address will take a lot of time.(Physical address) Thus we use IP. Else the routing table will be huge.
No. of IP's possible is the number of interfaces in your machine.
Proxy server - large bandwidth around 1TBPS. Have fixed number of ips.
Maintains a table. Keeps the local adress of the machine and physical address of the machine. When the local machine uses the internet the local address isn't visible.

Mobile communication
End user -> Access point/BTS is connected to the core network. -> core network -> access point -> end user
Core network is based on optical network.(Very fast). The nodes of this network are ISPs.
The node to which the end user is connected is called the edge node. The other nodes are called core nodes.
The most delay is from the access point to the end user not in the core network.
The core network is wired. The wireless transmission is only from end user to the access point.
Conference call?

networks are categorized into parts - Basic and Services(Client/Server based application)
Basic - routing algorithms.. applications.
1) Connections - wired or wireless - How to make RJ45
2) interface configuration - through terminal
3) Routing algo

Services
1) Socket programming
2) Ex of some popular socket based application - FTP, Telnet/SSH, DHCP
Socket programming - Server program works in a infinite loop (listening mode).. When a request comes.. the process creates a child process and gives the request to it. Now the child is responsible for the request to the particular client.

Changing the IP.
ifconfig interface_name new_ip_address
eg. ifconfig eth0 172.16.1.11
to reset the IP. or restart the interface
ifconfig eth0 down
ifconfig eth0 up
Broadcast address - is normally the network address and the last part is 255.

The ip address is called byte representative. IP address is of 4 bytes

To change the mask ifconfig netmask 255.255.255.0
if you change the mask it doesn't get reset even when you reset the connection. Gets reset when you restart the computer.